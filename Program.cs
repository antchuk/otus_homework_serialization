﻿using System;
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml.Linq;

namespace OTUS_homework_Serialization
{
    internal class Program
    {        
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            TestSerialization(10000);
            TestDeserialization(10000);
            Console.WriteLine("Press Enter to finish");
            Console.ReadLine();
        }
        private static void TestSerialization(int count)
        {
            MyTestSerialize("MySerializer.json", count);
            NewtonSerialize("NewtonSoftSerializer.json", count);
        }
        private static void TestDeserialization(int count)
        {
            MyTestDeSerialize("MySerializer.json", count);
            NewtonDeSerialize("NewtonSoftSerializer.json", count);
        }
        /// <summary>
        /// моя сериализация
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="count"></param>
        private static void MyTestSerialize(string filename, int count)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var _converter = MySerializer<FClass>.GetConverter();
            for (int i = 0; i < count; i++)
            {
                FClass SimpleUnit = new(true);
                string item = _converter.Serialize(SimpleUnit);
                File.WriteAllText(filename, item);
            }
            stopwatch.Stop();
            Console.WriteLine($"My serialization time = {stopwatch.Elapsed}");
        }
        /// <summary>
        /// моя десериализация
        /// </summary>
        /// <param name="file"></param>
        /// <param name="count"></param>
        private static void MyTestDeSerialize(string file, int count)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var _converter = MySerializer<FClass>.GetConverter();
            for (int i = 0; i < count; i++)
            {
                FClass SimpleUnit = _converter.Deserialize(file);
            }
            stopwatch.Stop();
            Console.WriteLine($"My DeSerialization time = {stopwatch.Elapsed}");
        }
        /// <summary>
        /// десериализация средствами NewtonSoft
        /// </summary>
        /// <param name="file"></param>
        /// <param name="count"></param>
        private static void NewtonDeSerialize(string file, int count)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < count; i++)
            {
                string jsonstring = File.ReadAllText(file);
                FClass SimpleUnit = Newtonsoft.Json.JsonConvert.DeserializeObject<FClass>(jsonstring);
            }
            stopwatch.Stop();
            Console.WriteLine($"NewtonSoft DeSerialization time = {stopwatch.Elapsed}");
        }
        /// <summary>
        /// сериализация средствами NewtonSoft
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="count"></param>
        private static void NewtonSerialize(string filename,int count)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < count; i++)
            {
                FClass SimpleUnit = new(true);
                string item = Newtonsoft.Json.JsonConvert.SerializeObject(SimpleUnit);
                File.WriteAllText(filename, item);
            }
            stopwatch.Stop();
            Console.WriteLine($"NewtonSoft serialization time = {stopwatch.Elapsed}");
        }
        /// <summary>
        /// сериализация средствами C#, работает (в отличии от десериализации)
        /// </summary>
        /// <param name="count"></param>
        private static void WinSerialize(int count)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < count; i++)
            {
                FClass SimpleUnit = new(true);
                var item = JsonSerializer.Serialize(SimpleUnit);
                File.WriteAllText("WinSerializer.json", item);
            }
            stopwatch.Stop();
            Console.WriteLine($"Win serialization time = {stopwatch.Elapsed}");
        }
    }
}