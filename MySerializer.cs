﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_homework_Serialization
{
    public class MySerializer<T>
    {
        private MySerializer() { }
        private static MySerializer<T> _instance;
        private static readonly object _locker = new object();
        public static MySerializer<T> GetConverter()
        {
            if (_instance == null)
            {
                lock (_locker)
                {
                    if (_instance == null) 
                    {
                        _instance = new MySerializer<T>();
                    }
                }
            }
            return _instance;
        }
        /// <summary>
        /// сериализует класс с набором свойств
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public string Serialize(T input)
        {
            var type = typeof(T);
            var fields = type.GetProperties();
            bool _ini = true;
            var strB = new StringBuilder();
            _ = strB.Append('{');
            foreach (var field in fields)
            {
                var _FiledName = field.Name;
                var _FieldValue = field.GetValue(input);
                if (_ini)
                {
                    _ = strB.Append('"');
                    _ = strB.Append(_FiledName);
                    _ = strB.Append("\":");
                    _ = strB.Append(_FieldValue);                    
                    _ini = false;
                }
                else
                {
                    _ = strB.Append(",\"");
                    _ = strB.Append(_FiledName);
                    _ = strB.Append("\":");
                    _ = strB.Append(_FieldValue);                    
                }
            }
            _ = strB.Append('}');
            return strB.ToString();
        }
        private void FillFields(ref FClass item, string _fname, string _fval)
        {
            var type = typeof(T);
            var fields = type.GetProperties();

            foreach (var _field in fields) 
            {
                if (_field.Name ==_fname)
                {
                    _ = int.TryParse(_fval, out int _ival);
                    item.GetType().GetProperty(_fname).SetValue(item, _ival);
                }
            }
        }
        /// <summary>
        /// Десриализует из файла(имя в параметре) в класс FClass
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns>FClass</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public FClass Deserialize(string FileName)
        {
            FClass ans = new(false);
            using (var reader = new StreamReader(FileName)) 
            {
                string line = reader.ReadToEnd() ?? throw new ArgumentNullException();
                line = line.Trim(new char[] { '}','{' });
                List<string> elements = line.Split(',').ToList();
                foreach ( var element in elements) 
                {
                    string _temp =element.Trim('"');
                    string _name = _temp.Substring(0,_temp.IndexOf(':')-1);
                    string _val = _temp.Substring(_temp.IndexOf(':')+1);
                    FillFields(ref ans, _name, _val);  
                }
            }
            return ans;
        }
    }
}
