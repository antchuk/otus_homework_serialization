﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_homework_Serialization
{
    //[Serializable]
    /// <summary>
    /// class F { int i1, i2, i3, i4, i5;}
    /// </summary>
    public class FClass
    {
        public int i1 {  get; set; }
        public int i2 {  get; set; }
        public int i3 {  get; set; }
        public int i4 { get; set; }
        public int i5 { get; set; }
        /// <summary>
        /// Создает класс class F { int i1, i2, i3, i4, i5;}
        /// </summary>
        /// <param name="initValues">true = значения по-умолчанию i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 </param>
        public FClass(bool initValues) 
        {
            if (initValues)
            {
                i1 = 1;
                i2 = 2;
                i3 = 3;
                i4 = 4;
                i5 = 5;
            }
        }
    }
}
